/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.midterm01_2;

/**
 *
 * @author nasan
 */
public class Human {

    private String name;//ตัวแปรสำหรับเก็บชื่อคน
    private int volOfdrink;//ตัวแปรสำหรับเก็บปริมาณที่จะดื่มน้ำ
    private int totalOfdrink = 0;//ตัวแปรที่จะเก็บปริมาณน้ำที่ดื่มไปทั้งหมด

    public Human(String name) {//constructor
        this.name = name;
    }

    public void drink(int volOfdrink, Glass glass) {//method ดื่มน้ำตามจำนวน volOfdrink และเลือกแล้วที่ต้องการดื่มโดยรับค่า glass 

        if (volOfdrink <= 0) {//กรณีใส่ค่าของการดื่มน้ำมาน้อยกว่าหรือเท่ากับ 0 จะไม่ทำการดื่ม
            System.out.println("Can't drink 0 ml. or less");//แสดงข้อความว่าไม่สามารถดื่มได้
        } 
        
        /*ด้านล่างเป็นกรณีใส่ค่าการดื่มมามากกว่าจำนวนน้ำที่มีอยู่ในแก้ว จะทำการเช็คตามเงื่อนไข
            หากค่าที่ใส่มานั้นน้อยกว่าหรือเท่ากับ 0 จะแสดงข้อความว่าไม่สามรถดื่มได้ น้ำหมด
            กรณีนอกเหนือจากนั้น(มีน้ำ) จะแสดงข้อความว่าไม่สามารถดื่มเกินน้ำที่มีอยู่ได้
            เมื่อแสดงข้อความแล้ว จะกำหนดน้ำที่จะกินให้เท่ากับน้ำที่มีอยู่(ให้กินจนหมด)
            จากนั้นเรียกใช้ method reduce เพื่อลดจำนวนน้ำในแก้วออกไป
            และแสดงข้อความว่าดื่มไปเท่าไหร่แล้ว และแสดงจำนวนน้ำในแก้ว*/ 
        
        else if (volOfdrink > glass.getVol()) {
            if (glass.getVol() <= 0) {
                System.out.println("Can't drink " + glass.getVol() + " ml. this glass is empty!!!");
            } else {
                System.out.println("Can't drink more than " + glass.getVol() + " ml. not enough water!!!");
                volOfdrink = glass.getVol(); //กำหนดน้ำที่จะกินให้เท่ากับน้ำที่มีอยู่
                glass.reduce(volOfdrink);//เรียกใช้ method reduce เพื่อลดจำนวนน้ำในแก้วออกไป
                totalOfdrink += volOfdrink;//บวกปริมาณน้ำที่ดื่มไปเพื่มสะสมเก็บจำนวนที่ดื่มไปทั้งหมด
                System.out.println(this.name + " Have already drink " + totalOfdrink + " ml. of water");
                System.out.println(glass);//แสดงข้อความว่าดื่มไปเท่าไหร่แล้ว
                System.out.println("________________________________");//แสดงเส้นปิดเมื่อจบการดื่ม
            }
        } else {
            this.volOfdrink = volOfdrink;
            glass.reduce(volOfdrink);//เรียกใช้ method reduce จาก class Glass

            totalOfdrink += volOfdrink;//เมื่อดื่มน้ำเสร็จให้เก็บปริมาณที่ดื่มไปไว้ใน totalOfdrink เพื่อจะเก็บปริมาณที่ดื่มไปทั้งหมด
            System.out.println(this.name + " Have already drink " + totalOfdrink + " ml. of water");//แสดงข้อความว่าคนนี้ดื่มไปแล้วกี่ ml.
            System.out.println(glass);//แสดงขื้อความบอกว่าแก้วนี้เหลือกี่ ml. จาก toString method ใน class glass
            System.out.println("________________________________");//แสดงเส้นปิดเมื่อจบการดื่ม
        }
    }
    
        public int getVolOfdrink() {
        return volOfdrink;
    }

    public int getTotalOfdrink() {
        return totalOfdrink;
    }

    public String getName() {
        return name;
    }

}
